class Logger {
    static log(message) {
        console.log(message);
    }

    static warn(message) {
        console.warn(message);
    }

    static info(message) {
        console.info(message);
    }
}

class DateNormalizer {
    getTimestamp(date) {
        var current = Math.round(date / 1000);
        var timeZone = (new Date()).getTimezoneOffset() * -60;
        return current - timeZone;
    }
}

class Fibonacci extends DateNormalizer {
    constructor() {
        super();
        this.initEvents();
    }

    get date1() {
        const current = new Date();

        if (this.currentMonth) {
            let firstDay = Date.UTC(current.getUTCFullYear(), current.getUTCMonth(), 1, 0, 0, 0, 0);
            return this.getTimestamp(firstDay);
        } else if (this.currentYear) {
            let firstDay = Date.UTC(current.getUTCFullYear(), 0, 1, 0, 0, 0, 0);
            return this.getTimestamp(firstDay);
        } else if (this.customDates) {
            let firstDay = new Date(document.querySelector('#initialTime').value);
            return this.getTimestamp(firstDay);
        }
    }

    get date2() {
        const current = new Date();

        if (this.currentMonth) {
            let lastDay = Date.UTC(current.getUTCFullYear(), current.getUTCMonth() + 1, 0, 23, 59, 59);
            return this.getTimestamp(lastDay);
        } else if (this.currentYear) {
            let lastDay = Date.UTC(current.getUTCFullYear(), 11, 31, 23, 59, 59);
            return this.getTimestamp(lastDay);
        } else if (this.customDates) {
            let lastDay = new Date(document.querySelector('#finalTime').value);
            return this.getTimestamp(lastDay);
        }
    }

    run() {
        let sequenceValidNumbers = [];
        let minLimit = this.date1;
        let maxLimit = this.date2;

        Logger.log('Initial limit: ' + minLimit);
        Logger.log('Final limit: ' + maxLimit);

        for (let i = minLimit; i <= maxLimit; i++) {
            if (Validator.isFibonacci(i)) {
                Logger.info(i + ' IS in Fibonacci');
                sequenceValidNumbers.push(i);
            } else {
                Logger.warn(i + ' NOT in Fibonacci');
            }
        }

        if (sequenceValidNumbers.length === 0) {
            this.output('There is not fibonacci sequence numbers.');
        }

        this.output('Fibonacci sequence: ' + sequenceValidNumbers.join(', '));
    }

    onSubmit(event) {
        event.preventDefault();
        this.setSelectedDateTimes();
        this.run();
    }

    setSelectedDateTimes() {
        this.currentMonth = document.querySelector('#timeRange1').checked;
        this.currentYear = document.querySelector('#timeRange2').checked;
        this.customDates = document.querySelector('#timeRange3').checked;
    }

    output(message) {
        document.querySelector('#outputResults').value = message;
    }

    initEvents() {
        document.querySelector('.form-fibonacci').addEventListener('submit', (event) => {
            this.onSubmit(event);
        });
    }
}

class Validator {
    static isPerfectSquare(n) {
        return n > 0 && Math.sqrt(n) % 1 === 0;
    };

    static isFibonacci(number) {
        return this.isPerfectSquare(5 * number * number + 4) || this.isPerfectSquare(5 * number * number - 4);
    }
}

let app = new Fibonacci();
