<?php

namespace Validators;

trait Fibonacci
{
    /**
     * Returns if number is in fibonacci sequence
     *
     * @param $number
     * @return bool
     */
    public static function isFibonacci($number): bool
    {
        $base = 5 * $number * $number;
        $test_low = $base + 4;
        $test_high = $base - 4;

        if (self::isPerfectSquare($test_low) || self::isPerfectSquare($test_high)) {
            return true;
        }

        return false;
    }

    /**
     * Returns if number has a perfect square
     *
     * @param $number
     * @return bool
     */
    private static function isPerfectSquare($number): bool
    {
        if (!is_int($number)) return false;

        $sqr = (int) sqrt($number);
        return $sqr * $sqr == $number;
    }
}