<?php

use Enums\DateTimeEnum;

/** Autoloader */
spl_autoload_extensions('.php');
spl_autoload_register(function($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    require $file;
});

/** Default timezone to UTC */
date_default_timezone_set(DateTimeEnum::UTC);

/** Checks and execution */
if (count($argv) == 1) {
    $commands = new Commands;
    $commands->help();
} else {
    try {
        $command = array_slice($argv, 1, 1)[0];
        $params = array_slice($argv, 2);

        $traces = preg_split('/:/', $command);
        $class = ucfirst($traces[0]);

        if (!isset($traces[1])) {
            $commands = new Commands;
            $helper = sprintf('help_%s', $class);
            $commands->$helper();
        } elseif ($traces[1] !== 'custom' && !isset($traces[2])) {
            $commands = new Commands;
            $helper = sprintf('help_%s_%s', $class, $traces[1]);
            $commands->$helper();
        } else {
            $execution = new $class();

            if (count($params)) {
                $execution->setCustomDates($params[0], $params[1]);
            } elseif ($traces[2] === 'month') {
                $execution->setCurrentMonthLimits();
            } elseif ($traces[2] === 'year') {
                $execution->setCurrentYearLimits();
            } else {
                $commands = new Commands();
                $helper = sprintf('help_%s_%s_%s', $class, $traces[1], $traces[2]);
                $command->$helper();
            }

            echo $execution->run();
        }
    } catch (\Exception $e) {
        echo "Its an error in execution.";
    }
}