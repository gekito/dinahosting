<?php

namespace Contracts;

interface FibonacciInterface
{
    public function run(): void;
    public function setCurrentMonthLimits(): void;
    public function setCurrentYearLimits(): void;
}