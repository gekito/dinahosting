<?php

use Enums\DateTimeEnum;
use Contracts\FibonacciInterface;
use Validators\Fibonacci as Validator;

class Fibonacci extends DateNormalizer implements FibonacciInterface
{
    use Validator;

    /**
     * @var $minLimit DateTime
     * @var $maxLimit DateTime
     */
    private $minLimit;
    private $maxLimit;

    /**
     * Execute sequence
     */
    public function run(): void
    {
        $return = [];
        $minLimit = $this->getTimestamp($this->minLimit);
        $maxLimit = $this->getTimestamp($this->maxLimit);

        for ($i = $minLimit; $i <= $maxLimit; $i++) {
            if (Validator::isFibonacci($i)) {
                $return[] = $i;
            }
        }

        if (!$return) {
            echo "There is not fibonnaci sequence numbers";
        } else {
            echo "Fibonnaci sequence: " . implode(', ', $return);
        }

        echo PHP_EOL;
    }

    /**
     * Set Limits to current month
     *
     * @throws Exception
     */
    public function setCurrentMonthLimits(): void
    {
        $minLimit = date('Y-m-01 0:00:00');
        $maxLimit = date("Y-m-t 23:59:59");

        $this->setMinAndMaxLimits($minLimit, $maxLimit);
    }

    /**
     * Set Limits to current year
     *
     * @throws Exception
     */
    public function setCurrentYearLimits(): void
    {
        $minLimit = date('Y-01-01 0:00:00');
        $maxLimit = date("Y-12-31 23:59:59");

        $this->setMinAndMaxLimits($minLimit, $maxLimit);
    }

    /**
     * Set Limits to custom dates provided in cli
     *
     * @param $date1
     * @param $date2
     * @throws Exception
     */
    public function setCustomDates($date1, $date2): void
    {
        $this->setMinAndMaxLimits($date1, $date2);
    }

    /**
     * @param $date1
     * @param $date2
     * @throws Exception
     */
    private function setMinAndMaxLimits($date1, $date2): void
    {
        $this->minLimit = $this->normalizeDate($date1, DateTimeEnum::UTC);
        $this->maxLimit = $this->normalizeDate($date2, DateTimeEnum::UTC);
    }
}