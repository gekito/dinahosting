<?php

class Commands
{
    const FIBONACCI_CURRENT_MONTH = 'Outputs fibonacci numbers included in current month';
    const FIBONACCI_CURRENT_YEAR = 'Outputs fibonacci numbers included in current year';
    const FIBONNACI_CUSTOM = 'Outputs fibonacci numbers included between two custom dates';

    public function help()
    {
        echo "\nEnabled commands in current cli:
        
    \033[0;32mfibonacci:current:month\033[0m         " . self::FIBONACCI_CURRENT_MONTH . "
    \033[0;32mfibonacci:current:year\033[0m          " . self::FIBONACCI_CURRENT_YEAR . "
    \033[0;32mfibonacci:custom date1 date2\033[0m    " . self::FIBONNACI_CUSTOM . "
        " . PHP_EOL;
    }

    public function help_fibonacci()
    {
        echo "\nEnabled commands in current namespace:
        
    \033[0;32mcurrent:month\033[0m         " . self::FIBONACCI_CURRENT_MONTH . "
    \033[0;32mcurrent:year\033[0m          " . self::FIBONACCI_CURRENT_YEAR . "
    \033[0;32mcustom date1 date2\033[0m    " . self::FIBONNACI_CUSTOM . "
        " . PHP_EOL;
    }

    public function help_fibonacci_current()
    {
        echo "\nEnabled commands in current namespace:
        
    \033[0;32mmonth\033[0m         " . self::FIBONACCI_CURRENT_MONTH . "
    \033[0;32myear\033[0m          " . self::FIBONACCI_CURRENT_YEAR . "
        " . PHP_EOL;
    }

    public function help_fibonacci_custom()
    {
        echo "\nEnabled commands in current namespace:
        
    \033[0;32mcustom date1 date2\033[0m    " . self::FIBONNACI_CUSTOM . "
        " . PHP_EOL;
    }
}