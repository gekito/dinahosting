<?php

class DateNormalizer
{
    /**
     * Converts date to DateTime object to better management
     *
     * @param string $dateTime
     * @param string $timeZone
     * @return DateTime
     * @throws Exception
     */
    public function normalizeDate(string $dateTime, $timeZone = 'UTC'): DateTime
    {
        $dateTime = new DateTime($dateTime);
        $dateTime->setTimezone(new DateTimeZone($timeZone));
        return $dateTime;
    }

    /**
     * Get timestamp int number from DateTime object
     *
     * @param DateTime $dateTime
     * @return int
     */
    public function getTimestamp(DateTime $dateTime): int
    {
        return $dateTime->getTimestamp();
    }
}