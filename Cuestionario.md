# Responde con tus propias palabras

- ¿Cuál fue el primer lenguaje de programación que empleaste?
Empecé con Pascal, programando en un IDE de su época llamado Delphi.
- ¿Cuál es tu principal lenguaje de programación actualmente?
PHP (un 90%)
- ¿Cuál es la diferencia entre un package.json y composer.json?
El composer.json se usa con composer para las librerías php (incluído frameworks), package.json para librerías de frontend o node en backend.
- ¿Qué es `/(<script(\s|\S)*?<\/script>)|(<style(\s|\S)*?<\/style>)|(<!--(\s|\S)*?-->)|(<\/?(\s|\S)*?>)/gi`?
Una expresión regular que captura todos los tags del html que son script, style, y cualquier otro (con sus atributos) y comentarios (pero sin el comentario en si). 
- ¿Para qué sirve la orden 'docker-compose up'?
Levantar los servicios incluidos en el docker-compose.yml del directorio en curso sin recompilar y en modo "foreground" (en primer plano).
- ¿Qué es el patrón MVC?
Un modelo de arquitectura de software que se basa en tres capas: Modelo, Vista, Controlador. El controlador recibe las cciones, que consulta al modelo, y se envía a la vista para su representación.
- ¿Indica algún Framework MVC y que aporta en el desarrollo de un proyecto?
CodeIgniter, Symfony (algo más complejo que un MVC puro, añadiendo el patrón Repository)... Simplicidad en el código, mejor lecutra y comprensióno del mismo, y multitud de rutinas ya controladas con el framework de las que no hay que preocuparse.
- ¿Qué es un ORM?
Es un framework de manejo de entidades de DB basado en objetos.
- ¿Indica algún ORM y que aporta en el desarrollo de un proyecto?
Illuminate en Laravel, o Doctrine en Symfony. Aporta el tratar los datos como entidades propias dentro de nuestro flujo, simplicidad de código, mejor lectura...
- ¿Qué es el patrón MVVM?
Es un patrón de diseño usado en frameworks de frontend (Modelo-Vista-Vista-Modelo), usado en los double data binding.
- ¿Indica algún Framework MVVM y que aporta en el desarrollo de un proyecto?
Angular, React, Vue... Igual que en el caso del backend, agiliza y simplifica el desarrollo.
- ¿Para qué sirve la orden 'git stash'?
Para almacenar los cambios actuales que tenemos en nuestro código sin necesidad de hacer un commit, por ejemplo, para cambiar de rama rápidamente para poder ver algo que se hizo en otra funcionalidad/parche, o porque descartamos los cambios que hemos hecho, pero no los queremos eliminar porque son interesantes en otro momento.
- ¿Indica que problemas identificas en el siguiente código?
```php
1 public function indexAction(Application $app, Request $request)
2 {
3    if ($app->getConfig('maintenance') && !in_array($request->getClientIp(), $app->getConfig('excludeIpsFromMaintenance'))) {
4        return $app['twig']->render('@MainLayouts/maintenance.html.twig', ['endDate' => $app->getConfig('maintenanceEndDate')]);
5    }
6	
7    try {
8        $databaseUser = $app->getDatabaseUser();
9        $user = new User($databaseUser);
10      
11        if (!$databaseUser->getProfile()->isInitialized()) {
12            $preferences = $this->getApp()->getEm()->getRepository('\CS\Entity\OcPreferences')->findOneBy(['userid' => $databaseUser->getUid(), 'configkey' => 'lang', 'appid' => 'core']);
13            if (null != $preferences) $form = $user->getInitForm($databaseUser->getProfile()->getDisplayname(), $preferences->getConfigvalue());
14        }
15  
16        return $app['twig']->render('@MainLayouts/init.html.twig', [ 'form' => $form->createView() ]);
17    } catch (\Exception $e) {
18        throw $e;
19    }
20 }
```
A modo apunte: ¿Silex quizás? Me suena el código, pero no he trabajado nunca con este framework. De todas maneras, un par de apuntes:
    - Se me hace raro que la instancia $app se pueda acceder como un array, aunque si tiene un interface tipo Arrayable sería totalmente válido.
	- En la línea 16 se usa el objeto $form, pero este se define en la línea 13, en el caso de que el el check de la linea 11 no sea un valor true (o compatible) se setea $form solo en el caso de que $preferences no sea null, dando un posible error cuando $preferences === null.
	- En el catch del try (linea 18), se está lanzando la misma excepción que se está interceptando sin hacer nada para evitar el posible error o mandar un mensaje de error al template o a la respues del servicio para mostrarlo en el frontend.